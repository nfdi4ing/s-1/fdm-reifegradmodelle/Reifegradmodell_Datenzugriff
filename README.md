# Maturity Model - Data Access


Es stehen zwei relevante Dateien zur Verfügung. 
Einmal [Übersichtsfolien](https://git.rwth-aachen.de/nfdi4ing/s-1/fdm-reifegradmodelle/Reifegradmodell_Datenzugriff/-/blob/main/RGM_Zugriff.pdf) zu der Phase des Datenzugriffs mit einleitenden Worten, zugehörigen Aktivitäten und dem entwickelten Inhalten des Reifegradmodells. Des Weiteren steht eine [Checkliste](https://git.rwth-aachen.de/nfdi4ing/s-1/fdm-reifegradmodelle/Reifegradmodell_Datenzugriff/-/blob/main/Checkliste_Zugriff.docx) zur Bewertung des Reifegrades zur Verfügung. Die Checkliste lässt sich innerhalb einzelner Forschungsprojekte für die Reifegradbewertung zur Umsetzung des Datenzugriffs einsetzen.
Jeder Reifegrad dieses Modells wurde eingehend Charakterisiert und für jeden Reifegrad wurden umzustetzende Ziele definiert. Bei Erfüllung aller Ziele eines Reifegrades wird einem diese Reifestufe attestiert.  

